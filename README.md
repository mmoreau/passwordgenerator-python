# Password Generator
Randomly generate passwords

## Dependencies

* Python 3 (https://www.python.org/downloads/)

## Function 

Password.Random(size, n)

* size (int) : Password length
* n (int) : Choice of characters (Upper-case alphabet, Lower-case alphabet, Number, Specials)
    * 0 : Upper-case alphabet + Lower-case alphabet + Number + Specials
    * 1 : Upper-case alphabet + Lower-case alphabet + Number
    * 2 : Upper-case alphabet + Lower-case alphabet
    * 3 : Upper-case alphabet
    * 4 : Lower-case alphabet + Number + Specials
    * 5 : Lower-case alphabet + Number
    * 6 : Lower-case alphabet
    * 7 : Number + Specials
    * 8 : Number
    * 9 : Specials

## Terminal 

### **Windows**

<pre>
    PasswordGenerator.py -s 12 -n 0
</pre>

### **Linux**

<pre>
    chmod +x PasswordGenerator.py
    ./PasswordGenerator.py -s 12 -n 0
</pre>

### **Mac**

<pre>
    python3 PasswordGenerator.py -s 12 -n 0
</pre>

## Script

In script.py / An example of use already made in the script

```python
    from PasswordGenerator import Password
    
    print(Password.Random(12, 0))
```

### **Windows**

<pre>
    script.py
</pre>

### **Linux**

<pre>
    chmod +x script.py
    ./script.py
</pre>

### **Mac**

<pre>
    python3 script.py
</pre>

## Contact me

Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered 

* **Mail :** mx.moreau1@gmail.com
* **Twitter :** mxmmoreau (https://twitter.com/mxmmoreau)