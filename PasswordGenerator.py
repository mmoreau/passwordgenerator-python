#!/usr/bin/python3

import random
import argparse
import sys


class Password:


    def __init__(self):
        pass


    @classmethod
    def Random(cls, size=12, n=0):

        lock = 0

        if isinstance(size, int):
            if size > 0:
                lock += 1


        if isinstance(n, int):
            if n in range(0, 10):
                lock += 1

        
        if lock == 2:
            
            character = (
                "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789&{([-_@)]}=+*$%!?",
                "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
                "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz",
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                "abcdefghijklmnopqrstuvwxyz0123456789&{([-_@)]}=+*$%!?",
                "abcdefghijklmnopqrstuvwxyz0123456789",
                "abcdefghijklmnopqrstuvwxyz",
                "0123456789&{([-_@)]}=+*$%!?",
                "0123456789",
                "&{([-_@)]}=+*$%!?"
            )      

            return "".join(random.choice(character[n]) for _ in range(size))


try:
    parser = argparse.ArgumentParser()
    
    parser.add_argument("-s", "--size", type=int)
    parser.add_argument("-n", "--number", type=int)

    args = parser.parse_args()

    _size = args.size
    _number = args.number

    if _size is not None and _number is not None:
        if _size > 0 and _number in range(0, 10):
            sys.stdout.write(Password.Random(_size, _number)+"\n")
except:
    pass